<?php 
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',  get_stylesheet_uri());

}




add_action( 'woocommerce_before_shop_loop_item_title', 'wpm_display_sold_out_loop_woocommerce' );// On l'affiche sur la page boutique
add_action( 'woocommerce_single_product_summary', 'wpm_display_sold_out_loop_woocommerce' );// On l'affiche sur la page du produit seul

 
function wpm_display_sold_out_loop_woocommerce() {
    global $product;
     //Si le produit est en rupture de stock, on affiche :
    if ( !$product->is_in_stock() ) {
        echo '<span class="soldout">' . __( 'Indisponible', 'woocommerce' ) . '</span>';
    }
} 